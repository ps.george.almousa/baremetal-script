#!/bin/bash
. ./0-env.sh
chmod -R 755 .
rm -rf logs/*
rm -rf artifacts/artifacts_ecc
rm -rf artifacts/artifacts_sec
rm -rf database/*
rm -rf $TOMCAT_PATH/webapps/*
rm -rf $TOMCAT_PATH/logs/*
rm -rf $TOMCAT_PATH/temp/*
rm -rf $TOMCAT_PATH/work/*
rm -rf war/*
rm -rf jopacc/artifacts*
mkdir -p  logs
mkdir -p  artifacts/artifacts_ecc
mkdir -p  artifacts/artifacts_sec
mkdir -p  database
mkdir -p  $TOMCAT_PATH/webapps
mkdir -p  $TOMCAT_PATH/logs
mkdir -p  $TOMCAT_PATH/temp
mkdir -p  $TOMCAT_PATH/work
mkdir -p database/ecc/DATABASE/
mkdir -p database/sec/DATABASE/
mkdir -p  jopacc/artifacts_ecc
mkdir -p  jopacc/artifacts_sec
sh 1-prepareartifacts.sh
sh 2-db-script.sh

echo " start Tomcat ..."
cd $TOMCAT_PATH/bin
##sh catalina.sh jpda run
sh startup.sh
tail -f ../logs/catalina.out &
sleep 50