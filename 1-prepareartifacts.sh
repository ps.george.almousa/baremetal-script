#!/bin/bash
set -e
echo '#################### prepare artifact !!! ################'
unzip ${BANK_ARTIFACT} -d artifacts/artifacts_ecc/
cp -rp artifacts/artifacts_ecc/DATABASE/* database/ecc/DATABASE/. 
unzip artifacts/artifacts_ecc/security/*.zip -d database/sec/
if [ "$INTG_ENABLED" = "true"  ]; then
unzip  artifacts/artifacts_ecc/*IntWar*.war -d $TOMCAT_PATH/webapps/intg
mv artifacts/artifacts_ecc/*IntWar*.war artifacts/artifacts_ecc/int.warcopied
fi
unzip  artifacts/artifacts_ecc/*.war  -d $TOMCAT_PATH/webapps/app
unzip  artifacts/artifacts_ecc/security/*.war  -d $TOMCAT_PATH/webapps/sec
cp -rp $TOMCAT_PATH/webapps/app $TOMCAT_PATH/webapps/ecc
  if [ "$INTG_ENABLED" = "true"  ]; then
cp -rp   $TOMCAT_PATH/webapps/intg $TOMCAT_PATH/webapps/eccintg
fi
unzip ${new_scanpage_war} -d tomcat/webapps/ROOT
sed -i 's|${app_user}|'$APP_USER'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|${app_password}|'$APP_USER'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|${sec_user}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|${sec_password}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|${db_url}|jdbc:oracle:thin:@'$eccdb_host'|g'  $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|${PSCI_FULL_DOMAIN}|'$PSCI_FULL_DOMAIN'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|${PSCI_FULL_DOMAIN_OUTWARD}|'$PSCI_FULL_DOMAIN_ZULL'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
sed -i 's|https://sun-imagecapture-master-image-capture-mock.ecc-dev.progressoft.cloud|http://localhost:'${mockimage_port}'|g' $TOMCAT_PATH/webapps/ROOT/assets/scanner-properties.json
sed -i 's|${PSCI_FULL_DOMAIN_OUTWARD}|'$PSCI_FULL_DOMAIN_ZULL'|g' $TOMCAT_PATH/webapps/ROOT/WEB-INF/classes/application-tomcat.yml
java -jar -Dserver.port=${mockimage_port} ${mockimage_path} &> ./mockimage_path.log &
if [ "$ENABLE_ECC" = "true" ]; then
sed -i 's|${MAIL_URL}|'$MAIL_URL'|g' $TOMCAT_PATH/webapps/sec/WEB-INF/classes/application.properties
sed -i 's|${DB_SEC_USERNAME}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/sec/META-INF/context.xml
sed -i 's|${DB_SEC_PASSWORD}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/sec/META-INF/context.xml
sed -i 's|${DB_URL}|jdbc:oracle:thin:@'$eccdb_host'|g' $TOMCAT_PATH/webapps/sec/META-INF/context.xml
sed -i 's|base href="/"|base href="/sec/"|g' $TOMCAT_PATH/webapps/sec/WEB-INF/classes/public/index.html
sed -i 's|${DB_URL}|jdbc:oracle:thin:@'$eccdb_host'|g' $TOMCAT_PATH/webapps/sec/META-INF/context.xml
sed -i 's|${DB_ECC_USERNAME}|'$APP_USER'|g' $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${DB_ECC_PASSWORD}|'$APP_PASSWORD'|g' $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${DB_SEC_USERNAME}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${DB_SEC_PASSWORD}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${DB_SHDW_USERNAME}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${DB_SHDW_PASSWORD}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${DB_URL}|jdbc:oracle:thin:@'$eccdb_host'|g'  $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${SEC_REST_URL}|http://'$ECC_SEC_URL'|g'  $TOMCAT_PATH/webapps/ecc/META-INF/context.xml
sed -i 's|${IS_MULTICLIENT}|'$IS_MULTICLIENT'|g' $TOMCAT_PATH/webapps/sec/WEB-INF/classes/application.properties
fi
if [ "$NEW_SCANPAGE_ENABLED" = "true" ]; then
 sed -i 's|${ZUUL_URL}|'$ZUUL_URL'|g' $TOMCAT_PATH/webapps/ecc/outward/LoadScan.jsp
 sed -i 's|${ZUUL_URL}|'$ZUUL_URL'|g' $TOMCAT_PATH/webapps/ecc/outward/LoadPostdatedScan.jsp
 sed -i 's|${NEW_OUTWARD_URL}|'$NEW_OUTWARD_URL'|g' $TOMCAT_PATH/webapps/ecc/outward/LoadScan.jsp
 sed -i 's|${NEW_OUTWARD_URL}|'$NEW_OUTWARD_URL'|g' $TOMCAT_PATH/webapps/ecc/outward/LoadPostdatedScan.jsp
fi
if [ "$INTG_ENABLED" = "true" ]; then
    sed -i 's|${APP_URL}|'$APP_URL'|g' $TOMCAT_PATH/webapps//WEB-INF/IncludedFiles/TagDirectives.jsp
    sed -i 's|${DB_ECC_USERNAME}|'$APP_USER'|g' $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${DB_ECC_PASSWORD}|'$APP_PASSWORD'|g' $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${DB_SEC_USERNAME}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${DB_SEC_PASSWORD}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${DB_SHDW_USERNAME}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${DB_SHDW_PASSWORD}|'$ECC_SEC_USER'|g' $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${DB_URL}|jdbc:oracle:thin:@'$eccdb_host'|g'  $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${SEC_REST_URL}|http://'$ECC_SEC_URL'|g'  $TOMCAT_PATH/webapps/eccintg/META-INF/context.xml
    sed -i 's|${INT_LOG_DIR}|'$INT_LOG_DIR'|g' $TOMCAT_PATH/webapps/eccintg/WEB-INF/integrationConfig.properties
    sed -i 's|${APP_NAME}|'$APP_NAME'|g' $TOMCAT_PATH/webapps/eccintg/WEB-INF/web.xml
    sed -i 's|${SIM_URL}|'$CORE_URL'|g' $TOMCAT_PATH/webapps/eccintg/WEB-INF/integrationConfig.properties

fi
rm -rf $TOMCAT_PATH/webapps/app $TOMCAT_PATH/webapps/intg 