#!/bin/bash
export SEC_DB_PATH=${PWD}/database/sec/DATABASE
export ECC_DB_PATH=${PWD}/database/ecc/DATABASE/ECC_CORE

 if [ "$ENABLE_ECC" = "true" ]; then

    echo "Installing SEC for ECC"
    cd $SEC_DB_PATH
    ./run_all.sh $ECC_SEC_USER $ECC_SEC_USER $CONNSTR $ROOT_DIR $SQLPLUS_PATH $TEMP_DIR $SYS_PASSWORD $RECREATE $IGNORE_STORAGE "$TABLESPACES_PATH" "$RELEASE_VERSION" "$MAIL_URL" "$IS_MULTICLIENT" "$ECC_DEF_BANK_CDS" "$SEC_PSCI_FULL_DOMAIN" "$SAMBA_ENABLED" "$SAMBA_URL" 

    echo "Installing APP for ECC"
    export ECC_USER_U=$ECC_USER
    export SHDW_USER=$ECC_SEC_USER
    cd $ECC_DB_PATH
    ./run_all.sh "ECC_APP15" "ECC_APP15" "ECC_SEC15" "ECC_SEC15" "10.51.3.123:1521/ECCTEST" . sqlplus /tmp "15" "false" oracle "true" "10" "true" '' "ECC_APP15" "ECC_APP15" "false" '/ecc/' '' "false" "false" "aaaa1234" "JORDAN" "false" "true" 'aaa' "aaaa1234" "true" "true" "false" '' '' '' "false" "false" "false"


    sqlplus $ECC_SEC_USER/$ECC_SEC_USER@$CONNSTR <<EOF
    update sec_app_instances set APPINST_INST_URL='/sec/' where app_id=1;
    update sec_app_instances set APPINST_INST_URL='/ecc/' where app_id=10;
    commit;
    exit;
EOF
    
 fi

echo "Your DB is up"


./run_all.sh "COM_APP09" "COM_APP09" "COM_SEC09" "COM_SEC09" "10.51.3.123:1521/ECCTEST" . sqlplus /tmp "9" "false" oracle "true" "10" "true" '' "COM_APP09" "COM_APP09" "false" "/com/" '' "false" "false" "aaaa1234" "JORDAN" "false" "true" 'aaa' "aaaa1234" "true" "true" "false" '' '' '' "false" "false" "false"

./run_all.sh "CEN_APP00" "CEN_APP00" "CEN_SEC00" "CEN_SEC00" "10.51.3.123:1521/ECCTEST" . sqlplus /tmp "0" "false" oracle "true" "10" "true" '' "CEN_APP00" "CEN_APP00" "false" "/cen/" '' "false" "false" "aaaa1234" "JORDAN" "false" "true" "aaa" "aaaa1234" "true" "true" "false" '' '' '' "false" "false" "false"

sqlplus CEN_SEC00/CEN_SEC00@10.51.3.123:1521/ECCTEST <<EOF
    update sec_app_instances set APPINST_INST_URL='/censec/' where app_id=1;
    update sec_app_instances set APPINST_INST_URL='/cen/' where app_id=10;
    commit;
    exit;
EOF



